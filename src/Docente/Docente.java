package Docente;

public class Docente {
    private double pagoBase;
    private int niv;
    private int horasTrabajadas;
    private int hijos;

    public Docente(double pagoBase, int nivel, int horasTrabajadas, int hijos) {
        this.pagoBase = pagoBase;
        this.niv = nivel;
        this.horasTrabajadas = horasTrabajadas;
        this.hijos = hijos;
    }

    public double calcularPago() {
        double pagoTotal = 0;

        if (niv == 1) {
            pagoTotal = pagoBase * 1.3f;
        } else if (niv == 2) {
            pagoTotal = pagoBase * 1.5f;
        } else if (niv == 3) {
            pagoTotal = pagoBase * 2.0f;
        }


        pagoTotal *= horasTrabajadas;

        return pagoTotal;
    }

    public double calcularImpuesto() {
        double pagoTotal = calcularPago();
        return pagoTotal * 0.16f;
    }

    public double calcularBono() {
        double bono = 0;

        if (hijos >= 1 && hijos <= 2) {
            bono = calcularPago() * 0.05f;
        } else if (hijos >= 3 && hijos <= 5f) {
            bono = calcularPago() * 0.1f;
        } else if (hijos > 5) {
            bono = calcularPago() * 0.2f;
        }

        return bono;
    }
}

