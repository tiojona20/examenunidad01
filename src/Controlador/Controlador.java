package Controlador;

import Docente.Docente;

public class Controlador {
    public static void main(String[] args) {
        double pagoBase = 10.0; 
        int nivel = 2; 
        int horasTrabajadas = 40;
        int hijos = 3; 

        Docente calculadora = new Docente(pagoBase, nivel, horasTrabajadas, hijos);

        double pago = calculadora.calcularPago();
        double impuesto = calculadora.calcularImpuesto();
        double bono = calculadora.calcularBono();

        System.out.println("Pago: " + pago);
        System.out.println("Impuesto: " + impuesto);
        System.out.println("Bono: " + bono);
    }
}
